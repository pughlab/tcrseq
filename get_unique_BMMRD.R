library(tcR)

source("get_unique_functions.R")

########
########
##BMMRD###
########
########

###
#generate CDR3 overlap file from all DNA samples

in.path <- "/Users/dmulder/Documents/tcell_clonality/projects/MiXCR/BMMRD"
fig.path <- "/Users/dmulder/Documents/tcell_clonality/projects/MiXCR/BMMRD_figs"
ref.count <- 20

#ref.file(in.path, fig.path)
###

##

#define list of sample files of interest
samples.sub <- c("X16_08_BMMRD.MMR63.1120",
                   "X16_08_BMMRD.MMR63.1121",
                   "X16_08_BMMRD.MMR101.1805.1",
                   "X16_08_BMMRD.MMR111.1755",
                   "X16_08_BMMRD.MMR117.1763",
                   "X16_08_BMMRD.MMR117.FFR2",
                   "X16_08_BMMRD.MMR120.1423",
                   "X16_08_BMMRD.MMR125.1762",
                   "X16_08_BMMRD.MMR128.1764",
                   "X16_08_BMMRD.MMR128.1804.2",
                   "X16_08_BMMRD.MMR128.1806",
                   "X16_08_BMMRD.MMR128.1807.1",
                   "X16_08_BMMRD.MMR128.X2.normal",
                   "X16_08_BMMRD.MMR128.X3",
                   "X16_08_BMMRD.MMR128.X5",
                   "X16_08_BMMRD.MMR128.X6",
                   "X16_08_BMMRD.MMR128.X7",
                   "X16_08_BMMRD.MMR139.1577",
                   "X16_08_BMMRD.MMR139.1578",
                   "X16_08_BMMRD.MMR152.JG")


samples.labels <- c("MMR63.1",
                    "MMR63.2",
                    "MMR101.1",
                    "MMR111.1",
                    "MMR117.1",
                    "MMR117.2",
                    "MMR120.1",
                    "MMR125.2",
                    "MMR128.3",
                    "MMR128.4",
                    "MMR128.5",
                    "MMR128.6",
                    "MMR128.7",
                    "MMR128.8",
                    "MMR128.9",
                    "MMR128.10",
                    "MMR128.11",
                    "MMR139.1",
                    "MMR139.2",
                    "MMR152.1")

ref.sample <- "BMMRD_all"

clonality.plot(in.path, ref.count, fig.path, samples.sub, samples.labels, ref.sample)

##
#define file name
subject_file_name <- "MMR63"

#define list of sample files of interest
subject_names <- c("X16_08_BMMRD.MMR63.1120",
                   "X16_08_BMMRD.MMR63.1121")

#overlap(in.path, ref.count, fig.path, subject_file_name, subject_names)

samples.sub <- c("X16_08_BMMRD.MMR63.1120",
                 "X16_08_BMMRD.MMR63.1121")


samples.labels <- c("MMR63.1",
                    "MMR63.2")

ref.name <- "X16_08_BMMRD.MMR63.1120"
ref.baseline <- "X16_08_BMMRD.MMR63.1120"
ref.sample <- "MMR63_all"

overlap.barplot(in.path, ref.count, fig.path, subject_file_name, subject_names, samples.sub, samples.labels, ref.name, ref.baseline, ref.sample)

##
#define file name
subject_file_name <- "MMR101"

#define list of sample files of interest
subject_names <- c("X16_08_BMMRD.MMR101.1805.1")

#overlap(in.path, ref.count, fig.path, subject_file_name, subject_names)

samples.sub <- c("X16_08_BMMRD.MMR101.1805.1")


samples.labels <- c("MMR101.1")

ref.name <- "X16_08_BMMRD.MMR101.1805.1"
ref.baseline <- "X16_08_BMMRD.MMR101.1805.1"
ref.sample <- "MMR101_all"

overlap.barplot(in.path, ref.count, fig.path, subject_file_name, subject_names, samples.sub, samples.labels, ref.name, ref.baseline, ref.sample)

##
#define file name
subject_file_name <- "MMR111"

#define list of sample files of interest
subject_names <- c("X16_08_BMMRD.MMR111.1755")

#overlap(in.path, ref.count, fig.path, subject_file_name, subject_names)

samples.sub <- c("X16_08_BMMRD.MMR111.1755")


samples.labels <- c("MMR111.1")

ref.name <- "X16_08_BMMRD.MMR111.1755"
ref.baseline <- "X16_08_BMMRD.MMR111.1755"
ref.sample <- "MMR111_all"

overlap.barplot(in.path, ref.count, fig.path, subject_file_name, subject_names, samples.sub, samples.labels, ref.name, ref.baseline, ref.sample)

##
#define file name
subject_file_name <- "MMR117"

#define list of sample files of interest
subject_names <- c("X16_08_BMMRD.MMR117.1763",
                   "X16_08_BMMRD.MMR117.FFR2")

#overlap(in.path, ref.count, fig.path, subject_file_name, subject_names)

samples.sub <- c("X16_08_BMMRD.MMR117.1763",
                 "X16_08_BMMRD.MMR117.FFR2")


samples.labels <- c("MMR117.1",
                    "MMR117.2")

ref.name <- "X16_08_BMMRD.MMR117.1763"
ref.baseline <- "X16_08_BMMRD.MMR117.1763"
ref.sample <- "MMR117_all"

overlap.barplot(in.path, ref.count, fig.path, subject_file_name, subject_names, samples.sub, samples.labels, ref.name, ref.baseline, ref.sample)

##
#define file name
subject_file_name <- "MMR120"

#define list of sample files of interest
subject_names <- c("X16_08_BMMRD.MMR120.1423")

#overlap(in.path, ref.count, fig.path, subject_file_name, subject_names)

samples.sub <- c("X16_08_BMMRD.MMR120.1423")


samples.labels <- c("MMR120.1")

ref.name <- "X16_08_BMMRD.MMR120.1423"
ref.baseline <- "X16_08_BMMRD.MMR120.1423"
ref.sample <- "MMR120_all"

overlap.barplot(in.path, ref.count, fig.path, subject_file_name, subject_names, samples.sub, samples.labels, ref.name, ref.baseline, ref.sample)

##
#define file name
subject_file_name <- "MMR125"

#define list of sample files of interest
subject_names <- c("X16_08_BMMRD.MMR125.1762")

#overlap(in.path, ref.count, fig.path, subject_file_name, subject_names)

samples.sub <- c("X16_08_BMMRD.MMR125.1762")


samples.labels <- c("MMR125.2")

ref.name <- "X16_08_BMMRD.MMR125.1762"
ref.baseline <- "X16_08_BMMRD.MMR125.1762"
ref.sample <- "MMR125_all"

overlap.barplot(in.path, ref.count, fig.path, subject_file_name, subject_names, samples.sub, samples.labels, ref.name, ref.baseline, ref.sample)

##
#define file name
subject_file_name <- "MMR128"

#define list of sample files of interest
subject_names <- c("X16_08_BMMRD.MMR128.1764",
                   "X16_08_BMMRD.MMR128.1804.2",
                   "X16_08_BMMRD.MMR128.1806",
                   "X16_08_BMMRD.MMR128.1807.1",
                   "X16_08_BMMRD.MMR128.X2.normal",
                   "X16_08_BMMRD.MMR128.X3",
                   "X16_08_BMMRD.MMR128.X5",
                   "X16_08_BMMRD.MMR128.X6",
                   "X16_08_BMMRD.MMR128.X7")

#overlap(in.path, ref.count, fig.path, subject_file_name, subject_names)

samples.sub <- c("X16_08_BMMRD.MMR128.1764",
                 "X16_08_BMMRD.MMR128.1804.2",
                 "X16_08_BMMRD.MMR128.1806",
                 "X16_08_BMMRD.MMR128.1807.1",
                 "X16_08_BMMRD.MMR128.X2.normal",
                 "X16_08_BMMRD.MMR128.X3",
                 "X16_08_BMMRD.MMR128.X5",
                 "X16_08_BMMRD.MMR128.X6",
                 "X16_08_BMMRD.MMR128.X7")


samples.labels <- c("MMR128.3",
                    "MMR128.4",
                    "MMR128.5",
                    "MMR128.6",
                    "MMR128.7",
                    "MMR128.8",
                    "MMR128.9",
                    "MMR128.10",
                    "MMR128.11")

ref.name <- "X16_08_BMMRD.MMR128.1764"
ref.baseline <- "X16_08_BMMRD.MMR128.1764"
ref.sample <- "MMR128_all"

overlap.barplot(in.path, ref.count, fig.path, subject_file_name, subject_names, samples.sub, samples.labels, ref.name, ref.baseline, ref.sample)

##
#define file name
subject_file_name <- "MMR139"

#define list of sample files of interest
subject_names <- c("X16_08_BMMRD.MMR139.1577",
                   "X16_08_BMMRD.MMR139.1578")

#overlap(in.path, ref.count, fig.path, subject_file_name, subject_names)

samples.sub <- c("X16_08_BMMRD.MMR139.1577",
                 "X16_08_BMMRD.MMR139.1578")


samples.labels <- c("MMR139.1",
                    "MMR139.2")

ref.name <- "X16_08_BMMRD.MMR139.1577"
ref.baseline <- "X16_08_BMMRD.MMR139.1577"
ref.sample <- "MMR139_all"

overlap.barplot(in.path, ref.count, fig.path, subject_file_name, subject_names, samples.sub, samples.labels, ref.name, ref.baseline, ref.sample)

##
#define file name
subject_file_name <- "MMR152"

#define list of sample files of interest
subject_names <- c("X16_08_BMMRD.MMR152.JG")

#overlap(in.path, ref.count, fig.path, subject_file_name, subject_names)

samples.sub <- c("X16_08_BMMRD.MMR152.JG")


samples.labels <- c("MMR152.1")

ref.name <- "X16_08_BMMRD.MMR152.JG"
ref.baseline <- "X16_08_BMMRD.MMR152.JG"
ref.sample <- "MMR152_all"

overlap.barplot(in.path, ref.count, fig.path, subject_file_name, subject_names, samples.sub, samples.labels, ref.name, ref.baseline, ref.sample)
