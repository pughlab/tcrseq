#script to extract mixcr log data
#use cat to append all your sample log files together
#for n in $(ls -d TLML*); do cat $n/*/logs/log* >> logs.txt; done

def logLoad(fileName):

    #declare our expected log data
    log_headers1 = ["Analysis Date",\
                   "Input file(s)",\
                   "Output file",\
                   "Version",\
                   "Analysis time",\
                   "Command line arguments"]
                   
    log_headers2 = ["Total sequencing reads"]
    
    log_headers3 = ["Successfully aligned reads", "Successfully aligned reads",\
                    "Chimeras","Chimeras",\
                    "Alignment failed, no hits (not TCR/IG?)","Alignment failed, no hits (not TCR/IG?)",\
                    "Alignment failed because of absence of V hits","Alignment failed because of absence of V hits",\
                    "Alignment failed because of absence of J hits","Alignment failed because of absence of J hits",\
                    "Alignment failed because of low total score","Alignment failed because of low total score",\
                    "Overlapped","Overlapped",\
                    "Overlapped and aligned","Overlapped and aligned",\
                    "Overlapped and not aligned","Overlapped and not aligned",\
                    "TRA chains","TRA chains",\
                    "TRB chains","TRB chains",\
                    "TRD chains","TRD chains",\
                    "TRG chains","TRG chains",\
                    "IGH chains","IGH chains",\
                    "IGK chains","IGK chains",\
                    "IGL chains","IGL chains"]
    
    log_headers4 = ["Analysis Date",\
                    "Input file(s)",\
                    "Output file",\
                    "Version",\
                    "Analysis time",\
                    "Command line arguments"]
    
    log_headers5 = ["Final clonotype count"]
    
    log_headers6 = ["Average number of reads per clonotype"]
    
    log_headers7 = ["Reads used in clonotypes, percent of total","Reads used in clonotypes, percent of total",\
                    "Reads used in clonotypes before clustering, percent of total","Reads used in clonotypes before clustering, percent of total",\
                    "Number of reads used as a core, percent of used","Number of reads used as a core, percent of used",\
                    "Mapped low quality reads, percent of used","Mapped low quality reads, percent of used",\
                    "Reads clustered in PCR error correction, percent of used","Reads clustered in PCR error correction, percent of used",\
                    "Reads pre-clustered due to the similar VJC-lists, percent of used","Reads pre-clustered due to the similar VJC-lists, percent of used",\
                    "Reads dropped due to the lack of a clone sequence","Reads dropped due to the lack of a clone sequence",\
                    "Reads dropped due to low quality","Reads dropped due to low quality",\
                    "Reads dropped due to failed mapping","Reads dropped due to failed mapping",\
                    "Reads dropped with low quality clones","Reads dropped with low quality clones"]
    
    log_headers8 = ["Clonotypes eliminated by PCR error correction",\
                    "Clonotypes dropped as low quality",\
                    "Clonotypes pre-clustered due to the similar VJC-lists"]
    
    log_headers9 = ["TRA chains","TRA chains",\
                    "TRB chains","TRB chains",\
                    "TRD chains","TRD chains",\
                    "TRG chains","TRG chains",\
                    "IGH chains","IGH chains",\
                    "IGK chains","IGK chains",\
                    "IGL chains","IGL chains"]
    
    #generate a single list of all the headers
    log_headers = log_headers1 + log_headers2 + log_headers3 + log_headers4 + log_headers5 + log_headers6 + log_headers7 + log_headers8 + log_headers9
    
    #declare an empty dummy list of expected types in case the log doesn't have an entry, note mixcr only reports a log line if found, ie: if no chimeras found then line is absent
    log_entries_empty = ["NA","NA","NA","NA","NA","NA",\
    0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    "NA","NA","NA","NA","NA","NA",\
    0,\
    0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,\
    0,\
    0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0,\
    0,0.0]
    
    
    #read cat assembled log file, should be sample 1 align log then sample 1 assemble log, then logs for next sample etc
    log = open(fileName,'r')

    #list storage
    log_final = []
    log_entries = log_entries_empty
    
    #flags to tell when we've moved on to a new log, separated by ======
    log_1 = False
    log_1_complete = False
    log_2 = False
    log_2_complete = False
    
    #for each line in the log
    for l in log:
        #cleanup whitespace and breaks
        l = l.strip()
        
        #only fill log 1 section if not yet done
        if not log_1_complete:
            #strings
            c=0
            for header in log_headers1:
                if l.find(header) != -1:
                    log_entries[c] = l.split(":")[1].strip()
                c += 1
            #integers
            c=6
            for header in log_headers2:
                if l.find(header) != -1:
                    log_entries[c] = int(l.split(":")[1].strip())
                c += 1
            #integer,float
            c=7
            for header in log_headers3:
                if l.find(header) != -1:
                    if type(log_entries[c]) == type(0):
                        log_entries[c] = int((l.split(":")[1]).split("(")[0].strip())
                    else:
                        log_entries[c] = float((l.split(":")[1]).split("(")[1].strip()[:-2])
                c += 1
                
        #only fill log2 section if log1 is done
        if log_1_complete:
            #strings
            c=39
            for header in log_headers4:
                if l.find(header) != -1:
                    log_entries[c] = l.split(":")[1].strip()
                c += 1
            #integers
            c=45
            for header in log_headers5:
                if l.find(header) != -1:
                    log_entries[c] = int(l.split(":")[1].strip())
                c += 1
            #floats
            c=46
            for header in log_headers6:
                if l.find(header) != -1:
                    log_entries[c] = float(l.split(":")[1].strip())
                c += 1
            #integers,floats
            c=47
            for header in log_headers7:
                if l.find(header) != -1:
                    if type(log_entries[c]) == type(0):
                        log_entries[c] = int((l.split(":")[1]).split("(")[0].strip())
                    else:
                        log_entries[c] = float((l.split(":")[1]).split("(")[1].strip()[:-2])
                c += 1
            #integers
            c=67
            for header in log_headers8:
                if l.find(header) != -1:
                    log_entries[c] = int(l.split(":")[1].strip())
                c += 1
            #integers,floats
            c=70
            for header in log_headers9:
                if l.find(header) != -1:
                    if type(log_entries[c]) == type(0):
                        log_entries[c] = int((l.split(":")[1]).split("(")[0].strip())
                    else:
                        log_entries[c] = float((l.split(":")[1]).split("(")[1].strip()[:-2])
                c += 1
        
        #this tells us whether we are on the align log or the assemble log
        if l.find("align -r") != -1:
            print("log 1 identified")
            log_1 = True
        elif l.find("assemble -r") != -1:
            print("log 2 identified")
            log_2 = True
        
        #here we hit a log separation line (===) so we know to end adding entries and move on to next sample
        if l.find("=====") != -1:
            print("log break detected")
            if log_1 and not log_2:
                log_1_complete = True
                print("log 1 complete")
            if log_1 and log_2:
                log_2_complete = True
                print("log 2 complete")
                
            if log_1_complete and log_2_complete:
                print("new sample trigger")
                log_final.append([log_entries])
                log_entries = log_entries_empty[:]
                log_1 = False
                log_2 = False
                log_1_complete = False
                log_2_complete = False

    log.close()
    
    return(log_headers, log_final)

#############
### main ####
#############

#call function to load and parse data
log_headers, log_final = logLoad("logs.txt")

#make sure all expected samples are here. If you mess up the initial log file it could terminate early
for entry in log_final:
    print(entry[0][1])
#    for sub_entry in entry:
#        print(sub_entry)

#write out data
f = open("mixcr_parsed_logs.txt", 'w')

#write headers
line = ""
for header in log_headers:
    line += (header + "\t")
line = line.strip()
f.write(line+"\n")

#write data
for sample in log_final:
    line = ""
    for entry in sample:
        for sub_entry in entry:
            line += (str(sub_entry) + "\t")
        line = line.strip()
        f.write(line+"\n")
f.close()