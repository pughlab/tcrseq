1. Run MiXCR on your data.

2. Rename the clones.txt file to your sample name.

3. Move all the renamed clones.txt files to a folder. If the filename starts with a number R will precede it with an "X". If the file name contains a dash, R will replace it will a ".". Best to not include either of these.

4. Functions are in get_unique_functions.R. Load this in to your project R script.

5. See examples of how the functions are called in get_unique_TLML.R.

6. Set all your paths in the project R script.

7. We need to generate an overlap file for all the clones in all the samples to make one giant master file. We start with this:

#define paths and ref.count which is number of samples (renamed clones.txt files in sample folder)

in.path <- "/Users/dmulder/Documents/tcell_clonality/projects/MiXCR/all"
fig.path <- "/Users/dmulder/Documents/tcell_clonality/projects/MiXCR/all_figs"
ref.count <- 177

#call tcR to generate clone overlap file
ref.file(in.path, fig.path)

#read in generated tcR overlap file
all_samples <- read.csv(paste(fig.path,"/CDR3_overlap.txt",sep=""),sep="\t",header=TRUE,fill=TRUE,na.strings="NA", stringsAsFactors=F, colClasses=c("character","character","integer",rep("integer",ref.count)))

8. Now we call the functions to generate figures for each sample collection. Here I am generating figures to illustrate all the TLML_001 samples which are a subset of the total 177 sample files in all_samples. See the example_figures folder for the types of figures that each function generates.

###
#define file name
subject_file_name <- "TLML_001"

#define list of sample files of interest, these are the names of the renamed clones.txt files. Note the "X" and "." modifications that I have made due to R's handling of the strings.
subject_names <- c("X16_08_TLML.001.AT_A_B_DNA",
                   "X16_08_TLML.001.AT_Infusion_L_DNA",
                   "X16_08_TLML.001.AT_C2D1_B_DNA",
                   "X16_08_TLML.001.AT_C2D4_B_DNA",
                   "TLML_1_AT_4W_TCR.J_Dep_TCR.V_genomic_1000",
                   "TLML_1_AT_4W_TCR.JV_cDNA_69",
                   "X16_08_TLML.001.AT_FU.01_B_DNA",
                   "X16_08_TLML.001.AT_FU.02_B_DNA",
                   "X16_08_TLML.001.AT_FU.03_B_DNA",
                   "X16_08_TLML.001.AT_FU.04_B_DNA",
                   "X16_08_TLML.001.AT_FU.05_B_DNA",
                   "X16_08_TLML.001.AT_FU.06_B_DNA",
                   "TLML_1_AT_FU7_TCR.J_Dep_TCR.V_genomic_1000",
                   "TLML_1_AT_FU7_TCR.JV_cDNA_18",
                   "X16_08_TLML.001.AT_FU.08_B_DNA",
                   "TLML_1_AT_FU9_TCR.J_Dep_TCR.V_genomic_1000",
                   "TLML_1_AT_FU9_TCR.JV_cDNA_13",
                   "TLML_1_AT_FU10_TCR.J_Dep_TCR.V_genomic_1000",
                   "TLML_1_AT_FU10_TCR.JV_cDNA_33",
                   "X16_08_TLML.001.AT_FU.011_B_DNA",
                   "TLML_1_AT_FU12_TCR.J_Dep_TCR.V_genomic_1000",
                   "TLML_1_AT_FU12_TCR.JV_cDNA_24",
                   "TLML_1_AT_FU13_TCR.J_Dep_TCR.V_genomic_1000",
                   "TLML_1_AT_FU13_TCR.JV_cDNA_21",
                   "X16_08_TLML.001.AT_FU.014_B_DNA",
                   "TLML_1_AT_FU15_TCR.J_Dep_TCR.V_genomic_1000",
                   "TLML_1_AT_FU15_TCR.JV_cDNA_19",
                   "TLML_1_AT_FU16_TCR.J_Dep_TCR.V_genomic_1000",
                   "TLML_1_AT_FU16_TCR.JV_cDNA_13")

#same as above due to some depreciated functions requirement.
samples.sub <- c("X16_08_TLML.001.AT_A_B_DNA",
                 "X16_08_TLML.001.AT_Infusion_L_DNA",
                 "X16_08_TLML.001.AT_C2D1_B_DNA",
                 "X16_08_TLML.001.AT_C2D4_B_DNA",
                 "TLML_1_AT_4W_TCR.J_Dep_TCR.V_genomic_1000",
                 "TLML_1_AT_4W_TCR.JV_cDNA_69",
                 "X16_08_TLML.001.AT_FU.01_B_DNA",
                 "X16_08_TLML.001.AT_FU.02_B_DNA",
                 "X16_08_TLML.001.AT_FU.03_B_DNA",
                 "X16_08_TLML.001.AT_FU.04_B_DNA",
                 "X16_08_TLML.001.AT_FU.05_B_DNA",
                 "X16_08_TLML.001.AT_FU.06_B_DNA",
                 "TLML_1_AT_FU7_TCR.J_Dep_TCR.V_genomic_1000",
                 "TLML_1_AT_FU7_TCR.JV_cDNA_18",
                 "X16_08_TLML.001.AT_FU.08_B_DNA",
                 "TLML_1_AT_FU9_TCR.J_Dep_TCR.V_genomic_1000",
                 "TLML_1_AT_FU9_TCR.JV_cDNA_13",
                 "TLML_1_AT_FU10_TCR.J_Dep_TCR.V_genomic_1000",
                 "TLML_1_AT_FU10_TCR.JV_cDNA_33",
                 "X16_08_TLML.001.AT_FU.011_B_DNA",
                 "TLML_1_AT_FU12_TCR.J_Dep_TCR.V_genomic_1000",
                 "TLML_1_AT_FU12_TCR.JV_cDNA_24",
                 "TLML_1_AT_FU13_TCR.J_Dep_TCR.V_genomic_1000",
                 "TLML_1_AT_FU13_TCR.JV_cDNA_21",
                 "X16_08_TLML.001.AT_FU.014_B_DNA",
                 "TLML_1_AT_FU15_TCR.J_Dep_TCR.V_genomic_1000",
                 "TLML_1_AT_FU15_TCR.JV_cDNA_19",
                 "TLML_1_AT_FU16_TCR.J_Dep_TCR.V_genomic_1000",
                 "TLML_1_AT_FU16_TCR.JV_cDNA_13")

#renaming the long string file names to short names for in the figure. Order will be same as above vector.
samples.labels <- c("Apheresis_DNA",
                    "Infusion_DNA",
                    "C2D1_DNA",
                    "C2D4_DNA",
                    "4W_DNA",
                    "4W_RNA",
                    "FU_01_DNA",
                    "FU_02_DNA",
                    "FU_03_DNA",
                    "FU_04_DNA",
                    "FU_05_DNA",
                    "FU_06_DNA",
                    "FU_07_DNA",
                    "FU_07_RNA",
                    "FU_08_DNA",
                    "FU_09_DNA",
                    "FU_09_RNA",
                    "FU_10_DNA",
                    "FU_10_RNA",
                    "FU_11_DNA",
                    "FU_12_DNA",
                    "FU_12_RNA",
                    "FU_13_DNA",
                    "FU_13_RNA",
                    "FU_14_DNA",
                    "FU_15_DNA",
                    "FU_15_RNA",
                    "FU_16_DNA",
                    "FU_16_RNA")

#define our reference samples. In this case I am using the infusion sample and the baseline sample.
ref.name <- "X16_08_TLML.001.AT_Infusion_L_DNA"
ref.baseline <- "Sample_16_08_TLML.001.AT_A_B_DNA_all"

#prefix for figures
ref.sample <- "TLML_001_all"

#vectors indicating sample subsets for some functions
names.tumour <- c("X16_08_TLML.001.AT_Infusion_L_DNA")
names.blood <- c("X16_08_TLML.001.AT_A_B_DNA",
                 "X16_08_TLML.001.AT_C2D1_B_DNA",
                 "X16_08_TLML.001.AT_C2D4_B_DNA",
                 "TLML_1_AT_4W_TCR.J_Dep_TCR.V_genomic_1000",
                 "TLML_1_AT_4W_TCR.JV_cDNA_69",
                 "X16_08_TLML.001.AT_FU.01_B_DNA",
                 "X16_08_TLML.001.AT_FU.02_B_DNA",
                 "X16_08_TLML.001.AT_FU.03_B_DNA",
                 "X16_08_TLML.001.AT_FU.04_B_DNA",
                 "X16_08_TLML.001.AT_FU.05_B_DNA",
                 "X16_08_TLML.001.AT_FU.06_B_DNA",
                 "TLML_1_AT_FU7_TCR.J_Dep_TCR.V_genomic_1000",
                 "TLML_1_AT_FU7_TCR.JV_cDNA_18",
                 "X16_08_TLML.001.AT_FU.08_B_DNA",
                 "TLML_1_AT_FU9_TCR.J_Dep_TCR.V_genomic_1000",
                 "TLML_1_AT_FU9_TCR.JV_cDNA_13",
                 "TLML_1_AT_FU10_TCR.J_Dep_TCR.V_genomic_1000",
                 "TLML_1_AT_FU10_TCR.JV_cDNA_33",
                 "X16_08_TLML.001.AT_FU.011_B_DNA",
                 "TLML_1_AT_FU12_TCR.J_Dep_TCR.V_genomic_1000",
                 "TLML_1_AT_FU12_TCR.JV_cDNA_24",
                 "TLML_1_AT_FU13_TCR.J_Dep_TCR.V_genomic_1000",
                 "TLML_1_AT_FU13_TCR.JV_cDNA_21",
                 "X16_08_TLML.001.AT_FU.014_B_DNA",
                 "TLML_1_AT_FU15_TCR.J_Dep_TCR.V_genomic_1000",
                 "TLML_1_AT_FU15_TCR.JV_cDNA_19",
                 "TLML_1_AT_FU16_TCR.J_Dep_TCR.V_genomic_1000",
                 "TLML_1_AT_FU16_TCR.JV_cDNA_13")
names.plasma <- NA

#declare dates for x-axes, in this case days since baseline
#dates <- c(-44,0,8,11,28,111,195,278,346,407,449,490,554,574,592,653,695,736,778,841,933)
dates <- c(-44,0,8,11,28,28,111,195,278,346,407,449,490,490,554,574,574,592,592,653,695,695,736,736,778,841,841,933,933)
# dates <- c("2013092401",
#            "2013100701",
#            "2013101501",
#            "2013101801",
#            "2013110501",
#            "2013110502",
#            "2014012801",
#            "2014042201",
#            "2014071501",
#            "2014092301",
#            "2014112401",
#            "2015010601",
#            "2015021701",
#            "2015021702",
#            "2015042101",
#            "2015051101",
#            "2015051102",
#            "2015062301",
#            "2015062302",
#            "2015082401",
#            "2015100601",
#            "2015100602",
#            "2015111701",
#            "2015111702",
#            "2015122901",
#            "2016030201",
#            "2016030202",
#            "2016060401",
#            "2016060402")

#function to generate figures
TCR.classes <- c("TRAV","TRBV","TRGV","TRDV")
alpha.prop <- overlap.compare7(all_samples, in.path, ref.count, fig.path, subject_file_name, subject_names, samples.sub, samples.labels, ref.name, ref.baseline, ref.sample, names.tumour, names.blood, names.plasma, dates, TCR.classes)

#function to generate figures
TCR.classes <- c(":TRAV",":TRBV",":TRGV",":TRDV","TRAV12-2")
top.clones <- overlap.compare5(all_samples, in.path, ref.count, fig.path, subject_file_name, subject_names, samples.sub, samples.labels, ref.name, ref.baseline, ref.sample, names.tumour, names.blood, names.plasma, dates, TCR.classes)
overlap.compare4(all_samples, in.path, ref.count, fig.path, subject_file_name, subject_names, samples.sub, samples.labels, ref.name, ref.baseline, ref.sample, names.tumour, names.blood, names.plasma)

#function to generate figures
clonality.plot(all_samples, in.path, ref.count, fig.path, samples.sub, samples.labels, ref.sample)

#function to generate figures
#overlap.compare(in.path, ref.count, fig.path, subject_file_name, subject_names, samples.sub, samples.labels, ref.name, ref.baseline, ref.sample)

#function to generate figures
overlap.compare2(all_samples, in.path, ref.count, fig.path, subject_file_name, subject_names, samples.sub, samples.labels, ref.name, ref.baseline, ref.sample, names.tumour, names.blood, names.plasma)

#function to generate figures
overlap.compare3(all_samples, in.path, ref.count, fig.path, subject_file_name, subject_names, samples.sub, samples.labels, ref.name, ref.baseline, ref.sample, names.tumour, names.blood, names.plasma)

#function to generate figures
cumulative.all(all_samples, in.path, ref.count, fig.path, subject_file_name, subject_names, samples.sub, samples.labels, ref.name, ref.baseline, ref.sample, names.tumour, names.blood, names.plasma)
